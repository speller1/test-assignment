<?php
const OWNER_EMAIL = 'aa@aa.aa';

include 'functions.php';

session_start();
$formToken = isset($_SESSION['token']) ? $_SESSION['token'] : ['token' => '', 'expires' => 0];
if (!$formToken['token']) {
    $formToken['token'] = (string)mt_rand();
}
$uiToken = isset($_POST['token']) ? $_POST['token'] : '';
if ($uiToken !== $formToken['token'] || $formToken['expires'] <= time()) {
    echo jsonError('Invalid security token. Reload the page and try again.');
    die;
}

// renew token expire time on each visit
$formToken['expires'] = time() + 60 * 30;
$_SESSION['token'] = $formToken;

$title = isset($_POST['title']) ? trim($_POST['title']) : '';
$text = isset($_POST['text']) ? trim($_POST['text']) : '';
$email = isset($_POST['email']) ? trim($_POST['email']) : '';
$pic = isset($_FILES['pic']) ? $_FILES['pic'] : null;

// Check input data for validity
if ($email != OWNER_EMAIL) {
    echo json_encode(['error' => 'Invalid email. You are not the owner.']);
    die;
}
if (!$title) {
    echo json_encode(['error' => 'Title not set.']);
    die;
}
if (!$text && !$pic) {
    echo json_encode(['error' => 'Text or image must be set.']);
    die;
}

$data = [
    'title' => $title,
    'text' => $text,
];

$dir = __DIR__ . '/../data/';
if (!is_dir($dir)) {
    mkdir($dir, 0777, true);
}
$id = 1;
do {
    $fn = $dir . $id . '.json';
    if ($exists = file_exists($fn)) {
        $id++;
    }
} while ($exists);

file_put_contents($dir . $id . '.json', json_encode($data));

$data['id'] = $id;
$data['date'] = time();
$data['dateFmt'] = date('Y/m/d H:i', time());

if ($pic) {
    $picDir = __DIR__ . '/pic';
    if (!is_dir($picDir)) {
        mkdir($picDir, 0777, true);
    }

    if ($pic['error'] == UPLOAD_ERR_OK) {
        $picFile = $picDir . "/{$id}.jpg";
        resize($pic['tmp_name'], $picFile, 300, 300, 85, true);
        $data['picUrl'] = "/pic/{$id}.jpg";
    }
}

$posts = getAllPosts(__DIR__ . '/../data', __DIR__ . '/pic', '/pic');
$words = countWordsInPosts($posts);
arsort($words);


echo json_encode([
    'post' => $data,
    'words' => array_slice($words, 0, 5),
]);
die();
