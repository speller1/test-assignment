<?php
include 'functions.php';
session_start();
$formToken = isset($_SESSION['token']) ? $_SESSION['token'] : ['token' => '', 'expires' => 0];
if (!$formToken['token']) {
	$formToken['token'] = (string)mt_rand();
}
// renew token expire time on each visit
$formToken['expires'] = time() + 60 * 30;
$_SESSION['token'] = $formToken;

if (isset($_GET['get-posts'])) {
	$posts = getAllPosts(__DIR__ . '/../data', __DIR__ . '/pic', '/pic');
	usort(
		$posts,
		function ($a, $b)
		{
            $av = $a->date;
            $bv = $b->date;
            if ($av > $bv) {
                return 1;
            } elseif ($bv > $av) {
                return -1;
            } else {
                return 0;
            }
		}
	);

	$words = countWordsInPosts($posts);
    arsort($words);

	echo json_encode([
        'posts' => $posts,
        'words' => array_slice($words, 0, 5),
    ]);

	die;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Albelli</title>

	<link rel="stylesheet" href="//cdn-files.cloud/arc/css/arc.discovery.min.css"/>
	<link rel="stylesheet" href="/style.css"/>
</head>
<body>
<div class="content">
	<div class="panel header">
		<h1>My spectacular blog</h1>
		<p>A totally false statement</p>
	</div>
	<div class="panel blog-post-form">
		<div class="row">
			<div class="large-8 column">
				<h3>New blog post</h3>
			</div>
		</div>
		<form id="post-form" class="">
			<div class="row">
				<div class="large-8 column fields-block">
					<input name="title" placeholder="My post title" type="text">
					<textarea name="text" placeholder="Here all my important post text!"></textarea>
					<input name="email" placeholder="Email Address" type="email">
					<input name="pic" type="file">
					<input type="hidden" name="token" value="<?= $formToken['token'] ?>">
				</div>
				<div class="large-3 column btn-block">
					<button class="small button">Save post</button>
				</div>
			</div>
		</form>
	</div>
    <div class="posts-block">
        <div class="most-used">
            <div class="panel">
                <span>Most used words here</span>
                <ul id="words">
                </ul>
            </div>
        </div>
        <div class="posts" id="posts">
        </div>
    </div>
</div>
</body>

<script id="post-tpl" type="text/x-handlebars-template">
	<div class="panel post">
		<div class="post-head">
			<div class="title">
				<h3>{{ title }}</h3>
			</div>
			<div class="date">
				<span class="date">{{ dateFmt }}</span>
			</div>
		</div>
		<div class="text">
			{{#if picUrl}}
			<img src="{{ picUrl }}" align="left"/>
			{{/if}}
			<p>{{ text }}</p>
		</div>
	</div>
</script>

<script type='text/javascript' src="//cdn-files.cloud/arc/js/jquery.arc.min.js"></script>
<script type='text/javascript' src="//cdn-files.cloud/arc/js/picturefill.arc.min.js"></script>
<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.11/handlebars.min.js"></script>
<script type='text/javascript' src="/js.js"></script>

</html>