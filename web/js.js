$(function () {

    (function (window, Handlebars, $) {

        var postForm = $('#post-form'),
            postsContainer = $('#posts');

        postForm.on('submit', function (event) {
            event.preventDefault();
            var formData = new FormData(this);
            if (!checkForm(formData)) {
                alert('There are errors in the form');
                return;
            }
            $.ajax({
                url: '/post.php',
                type: 'POST',
                data: formData,
                success: function (data) {
                    data = JSON.parse(data);
                    stopProgress(postForm);
                    if (data.error) {
                        alert(data.error);
                    } else {
                        addNewPost(data.post);
                        postForm.find('[name=title]').val('');
                        postForm.find('[name=text]').val('');
                        postForm.find('[name=email]').val('');
                        updateWords(data.words);
                    }
                },
                error: function () {
                    alert('Error posting data');
                    stopProgress(postForm);
                },
                cache: false,
                contentType: false,
                processData: false
            });
            startProgress(postForm);
        });

        function loadInitialPosts() {
            $.ajax({
                url: '?get-posts',
                type: 'GET',
                data: [],
                success: function (data) {
                    stopProgress(postsContainer);
                    data = JSON.parse(data);
                    for (var i in data.posts) {
                        addNewPost(data.posts[i]);
                    }
                    updateWords(data.words);
                },
                error: function () {
                    alert('Error posting data');
                    stopProgress(postsContainer);
                },
                cache: false,
                contentType: false,
                processData: false
            });
            startProgress(postsContainer);
        }

        loadInitialPosts();


        function addNewPost(data) {
            var tpl = Handlebars.compile($('#post-tpl').html());
            var html = tpl(data);
            $('#posts').prepend(html);
        }

        function updateWords(data) {
            var words = $('#words');
            words.empty();
            for (var word in data) {
                var item = $('<li>');
                item.text(word);
                words.append(item);
            }
        }

        function checkForm(formData) {
            var hasErrors = false, hasError;
            var title = formData.get('title').trim();
            setFieldHasError('title', hasError = (title.length === 0));
            hasErrors = hasErrors || hasError;

            var text = formData.get('text').trim();
            var pic = formData.get('pic');
            setFieldHasError('text', hasError = (text.length === 0 && pic.size === 0));
            hasErrors = hasErrors || hasError;

            var email = formData.get('email').trim();
            setFieldHasError('email', hasError = (email.length === 0) || !validateEmail(email));
            hasErrors = hasErrors || hasError;

            return !hasErrors;
        }

        function setFieldHasError(fieldName, hasError) {
            var f = postForm.find('[name=' + fieldName + ']');
            if (hasError) {
                f.addClass('error');
            } else {
                f.removeClass('error');
            }
            return hasError;
        }

        function startProgress(el) {
            $(el).addClass('ajax-progress');
        }

        function stopProgress(el) {
            $(el).removeClass('ajax-progress');
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

    })(window, Handlebars, $);

});
