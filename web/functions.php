<?php

function countWordsInPosts(array $posts): array
{
    $result = [];
    foreach ($posts as $post) {
        foreach (countWords($post->text . ' ' . $post->title) as $word => $counter) {
            $result[$word] = isset($result[$word]) ? $result[$word] + $counter : $counter;
        }
    }
    return $result;
}

/**
 * Count words and return array word => count
 * @param string $text
 * @return array
 */
function countWords(string $text): array
{
    $text = mb_strtolower($text);
    $text = preg_replace('![^\w]!', ' ', $text);
    $text = array_filter(explode(' ', $text));
    $result = [];
    foreach ($text as $word) {
        if (strlen($word) > 4) {
            $result[$word] = isset($result[$word]) ? $result[$word]++ : 1;
        }
    }
    return $result;
}

/**
 * Resize to the specified dimensions
 * If $noCrop = false the picture will be truncated to fit the dimensions.
 * @param string $srcFile
 * @param string $destFile
 * @param int $width
 * @param int $height
 * @param int $quality
 * @param bool $noCrop
 * @throws \Exception
 */
function resize($srcFile, $destFile, $width, $height, $quality, $noCrop)
{
    $width = (int)$width;
    $height = (int)$height;
    $quality = (int)$quality;
    $cropCmd = '';
    // obtain the source size
    list($srcWidth, $srcHeight) = getimagesize($srcFile);
    if (!$srcWidth || !$srcHeight) {
        throw new \InvalidArgumentException("File `$srcFile` is not an image");
    }
    // Obtain src ratio
    $srcRatio = $srcWidth / $srcHeight;
    // Obtain dest ratio
    $destRatio = $width / $height;
    // Check we need the ratio modification
    if (!$noCrop && $srcRatio != $destRatio) {
        $offsetWidth = $offsetHeight = 0;
        // Obtain crop frame size and offset matching the requested ratio
        if ($srcRatio < $destRatio) {
            $cropWidth = $srcWidth;
            $cropHeight = (int)($srcWidth / $destRatio);
            $offsetHeight = (int)(($srcHeight - $cropHeight) / 2);
        } else {
            $cropHeight = $srcHeight;
            $cropWidth = (int)($srcHeight * $destRatio);
            $offsetWidth = (int)(($srcWidth - $cropWidth) / 2);
        }
        $cropCmd = " -crop {$cropWidth}x{$cropHeight}+{$offsetWidth}+{$offsetHeight}";
    }
    if ($width > $srcWidth && $height > $srcHeight) {
        // Required size is larger than source. Dont resize, just convert
        $destWidth = $srcWidth;
        $destHeight = $srcHeight;
    } else {
        $destWidth = $width;
        $destHeight = $height;
    }
    $srcFile = escapeFileName($srcFile);
    $destFile = escapeFileName($destFile);
    $bg = '-background white -alpha background'; // add white background instead of transparency
    // crop and resize the image
    $cmd = "convert {$srcFile} -layers flatten -auto-orient{$cropCmd} -resize {$destWidth}x{$destHeight} {$bg} -quality {$quality} -strip {$destFile}";
    exec($cmd, $out, $ret);
    if ($ret) {
        throw new \Exception('convert error (' . $ret . '): ' . $cmd);
    }
}

/**
 * Escape file name
 * @param string $fn
 * @return string
 */
function escapeFileName(string $fn): string
{
    $fn = str_replace('\\', '/', $fn);
    return escapeshellarg($fn);
}

/**
 * Returns formatted json string with error text
 * @param string $text
 * @return string
 */
function jsonError(string $text): string
{
    return json_encode(['error' => 'Invalid security token. Reload the page and try again.']);
}

/**
 * Get all posts data
 * @param string $postDir
 * @param string $picDir
 * @param string $picUrl
 * @return array
 */
function getAllPosts(string $postDir, string $picDir, string $picUrl): array
{
	$posts = [];
	foreach (glob($postDir . '/*.json') as $fileOrDir) {
		if ($fileOrDir == '..' || $fileOrDir == '.' || is_dir($fileOrDir)) {
			continue;
		}
		$postData = json_decode(file_get_contents($fileOrDir));
		$postData->date = $t = filemtime($fileOrDir);
		$postData->dateFmt = date('Y/m/d H:i', $t);
		$id = (int)str_replace('.json', '', basename($fileOrDir));
		$picFile = "{$picDir}/{$id}.jpg";
		if (file_exists($picFile)) {
		    $postData->picUrl = "{$picUrl}/{$id}.jpg";
        }
        $postData->id = $id;
		$posts[] = $postData;
	}
    return $posts;
}